package com.plb.parametersjump;

public class JumpsLogic {
    
    public static void jumpToNextItem(JumpExecutor jumper, int initialPosition, String text) {
        
        // find brackets
        BracketInfo brackets = SourceAnalyzer.findBracketsAndComasAround(text, initialPosition);
        if (null == brackets) {
            brackets = SourceAnalyzer.findBracketsAndComasRight(text, initialPosition);
            if (null == brackets) {
                return;
            }
        }

        // find current item index
        Integer currentItem = brackets.getItemIndexByPosition(text, initialPosition);

        // find new item position
        int nextItem = null != currentItem
                ? currentItem + 1
                : -1;
        BracketItemInfo nextItemPosition = brackets.getItemPosition(text, nextItem);
        
        // jump
        jumper.jump(nextItemPosition.getJumpPosition());
    }

    public static void jumpToNextItemWithRecursion(JumpExecutor jumper, int initialPosition, String text) {
        // find brackets
        BracketInfo brackets = SourceAnalyzer.findBracketsAndComasAround(text, initialPosition);
        if (null == brackets) {
            brackets = SourceAnalyzer.findBracketsAndComasRight(text, initialPosition);
            if (null == brackets) {
                return;
            }
        } else {
            BracketInfo innerBrackets = SourceAnalyzer.findBracketsAndComasRight(text, initialPosition, brackets.getClosePosition());
            if (null != innerBrackets) {
                brackets = innerBrackets;
            }
        }
        
        // find current item index
        Integer currentItem = brackets.getItemIndexByPosition(text, initialPosition);

        // find new item position
        int nextItem = null != currentItem
                ? currentItem + 1
                : -1;
        BracketItemInfo nextItemPosition = brackets.getItemPosition(text, nextItem);

        // jump
        jumper.jump(nextItemPosition.getJumpPosition());
    }

    public static void jumpForwardToCertainItem(JumpExecutor jumper, int initialPosition, String text, int itemIndex) {

        // find brackets
        BracketInfo brackets = SourceAnalyzer.findBracketsAndComasAround(text, initialPosition);
        if (null == brackets) {
            brackets = SourceAnalyzer.findBracketsAndComasRight(text, initialPosition);
            if (null == brackets) {
                return;
            }
        }

        // don't jump to item after brackets
        if (itemIndex >= brackets.getLastItemIndex()) {
            return;
        }
        // find required item position
        BracketItemInfo itemPosition = brackets.getItemPosition(text, itemIndex);

        // jump
        jumper.jump(itemPosition.getJumpPosition());
    }

    public static void jumpToPrevItem(JumpExecutor jumper, int initialPosition, String text) {
        // find brackets
        BracketInfo brackets = SourceAnalyzer.findBracketsAndComasAround(text, initialPosition);
        if (null == brackets) {
            brackets = SourceAnalyzer.findBracketsAndComasLeft(text, initialPosition);
            if (null == brackets) {
                return;
            }
        }

        // find current item index
        Integer currentItem = brackets.getItemIndexByPosition(text, initialPosition);

        // find new item position
        int nextItem = null != currentItem
                ? currentItem - 1
                : brackets.getLastItemIndex() - 1;
        BracketItemInfo nextItemPosition = brackets.getItemPosition(text, nextItem);

        // jump
        jumper.jump(nextItemPosition.getJumpPosition());
    }

    public static void jumpToPrevItemWithRecursion(JumpExecutor jumper, int initialPosition, String text) {
        // find brackets
        BracketInfo brackets = SourceAnalyzer.findBracketsAndComasAround(text, initialPosition);
        if (null == brackets) {
            brackets = SourceAnalyzer.findBracketsAndComasLeft(text, initialPosition);
            if (null == brackets) {
                return;
            }
        } else {
            BracketInfo innerBrackets = SourceAnalyzer.findBracketsAndComasLeft(text, initialPosition, brackets.getOpenPosition());
            if (null != innerBrackets) {
                brackets = innerBrackets;
            }
        }

        // find current item index
        Integer currentItem = brackets.getItemIndexByPosition(text, initialPosition);

        // find new item position
        int nextItem = null != currentItem
                ? currentItem - 1
                : brackets.getLastItemIndex() - 1;
       BracketItemInfo nextItemPosition = brackets.getItemPosition(text, nextItem);

        // jump
        jumper.jump(nextItemPosition.getJumpPosition());
    }

    public static void jumpToItemBeforeAssign(JumpExecutor jumper, int initialPosition, String text) {

        int leftBorder = SourceAnalyzer.getLeftAnalyzingBorder(text, initialPosition, true);
        int rightBorder = SourceAnalyzer.getRightAnalyzingBorder(text, initialPosition, true);
        Integer equalsSignPosition = SourceAnalyzer.findSymbolLeft(text, initialPosition, leftBorder, '=');
        if (null == equalsSignPosition) {
            equalsSignPosition = SourceAnalyzer.findSymbolRight(text, initialPosition, rightBorder, '=');
        }

        if (null == equalsSignPosition) {
            return;
        }

        leftBorder = SourceAnalyzer.getLeftAnalyzingBorder(text, equalsSignPosition, true);
        int firstWordPositionLeft = SourceAnalyzer.findFirstWordPositionLeft(text, leftBorder, equalsSignPosition - 1);

        // jump
        jumper.jump(firstWordPositionLeft);
    }
}