package com.plb.parametersjump.actions;

import com.plb.parametersjump.JumpExecutor;
import com.plb.parametersjump.JumpsLogic;


public class JumpRecursiveToNextItem extends AbstractJumpAction {

    @Override
    public void jumpAction(JumpExecutor jumpExecutor, int initialOffset, String text) {
        JumpsLogic.jumpToNextItemWithRecursion(jumpExecutor, initialOffset, text);
    }

}
