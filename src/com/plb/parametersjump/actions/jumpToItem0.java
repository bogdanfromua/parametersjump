package com.plb.parametersjump.actions;


import com.plb.parametersjump.JumpExecutor;
import com.plb.parametersjump.JumpsLogic;

public class jumpToItem0 extends AbstractJumpAction {

    @Override
    public void jumpAction(JumpExecutor jumpExecutor, int initialOffset, String text) {
        JumpsLogic.jumpForwardToCertainItem(jumpExecutor, initialOffset, text, 0);
    }
}
