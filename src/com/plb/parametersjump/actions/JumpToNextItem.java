package com.plb.parametersjump.actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataConstants;
import com.intellij.openapi.editor.CaretModel;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.ex.DocumentEx;
import com.plb.parametersjump.JumpExecutor;
import com.plb.parametersjump.JumpsLogic;


public class JumpToNextItem extends AbstractJumpAction {

    @Override
    public void jumpAction(JumpExecutor jumpExecutor, int initialOffset, String text) {
        JumpsLogic.jumpToNextItem(jumpExecutor, initialOffset, text);
    }
}
