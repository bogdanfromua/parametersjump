package com.plb.parametersjump.actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataConstants;
import com.intellij.openapi.editor.CaretModel;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.ex.DocumentEx;
import com.plb.parametersjump.JumpExecutor;

public abstract class AbstractJumpAction extends AnAction {
    @Override
    public void actionPerformed(AnActionEvent anActionEvent) {
        Editor editor = (Editor)anActionEvent.getDataContext().getData(DataConstants.EDITOR);
        CaretModel caretModel = editor.getCaretModel();
        int initialOffset = caretModel.getPrimaryCaret().getOffset();
        final DocumentEx doc = (DocumentEx) editor.getDocument();
        String text = doc.getText();
        
        jumpAction(new JumpExecutor(editor, caretModel), initialOffset, text);
    }
    
    public abstract void jumpAction(JumpExecutor jumpExecutor, int initialOffset, String text);
}
