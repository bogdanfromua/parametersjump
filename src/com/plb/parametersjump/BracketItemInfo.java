package com.plb.parametersjump;

public class BracketItemInfo {
    private int fromPosition; //inclusive
    private int toPosition; //inclusive
    private int jumpPosition;

    public BracketItemInfo(int fromPosition, int toPosition, int jumpPosition) {
        this.fromPosition = fromPosition;
        this.toPosition = toPosition;
        this.jumpPosition = jumpPosition;
    }

    public int getFromPosition() {
        return fromPosition;
    }

    public void setFromPosition(int fromPosition) {
        this.fromPosition = fromPosition;
    }

    public int getToPosition() {
        return toPosition;
    }

    public void setToPosition(int toPosition) {
        this.toPosition = toPosition;
    }

    public int getJumpPosition() {
        return jumpPosition;
    }

    public void setJumpPosition(int jumpPosition) {
        this.jumpPosition = jumpPosition;
    }
}
