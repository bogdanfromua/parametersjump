package com.plb.parametersjump;

import java.util.ArrayList;
import java.util.List;

class BracketInfo {
    private int openPosition;
    private int closePosition;
    private List<Integer> comaPositions = new ArrayList<>();

    public BracketInfo(int openPosition, int closePosition, List<Integer> comaPositions) {
        this.openPosition = openPosition;
        this.closePosition = closePosition;
        this.comaPositions = comaPositions;
    }

    public int getOpenPosition() {
        return openPosition;
    }

    public void setOpenPosition(int openPosition) {
        this.openPosition = openPosition;
    }

    public int getClosePosition() {
        return closePosition;
    }

    public void setClosePosition(int closePosition) {
        this.closePosition = closePosition;
    }

    public List<Integer> getComaPositions() {
        return comaPositions;
    }

    public void setComaPositions(List<Integer> comaPositions) {
        this.comaPositions = comaPositions;
    }

    /**
     * 
     * @param itemIndex
     *  ...-1(0,1,2,3,4)5...
     * @return
     *  from-to pair (from - inclusively, to - exclusively) 
     */
    public BracketItemInfo getItemPosition(String text, int itemIndex) {
        boolean isInsideBrackets = false;

        final int from;
        if (-1 >= itemIndex) {
            from = getItemBeforeBracketsStartPosition(text);
        } else if (0 == itemIndex) {
            from = openPosition + 1;
            isInsideBrackets = true;
        } else if (itemIndex <= comaPositions.size()) {
            from = comaPositions.get(itemIndex - 1) + 1;
            isInsideBrackets = true;
        } else {
            from = closePosition + 1;
        }

        final int to;
        if (-1 >= itemIndex) {
            to = openPosition;
        } else if (itemIndex < comaPositions.size()) {
            to = comaPositions.get(itemIndex) -1;
        } else if (comaPositions.size() == itemIndex) {
            to = closePosition;
        } else {
            to = closePosition + 1;
        }

        final int jumpPosition = SourceAnalyzer.findBestParameterJumpPosition(text, from, to, isInsideBrackets);

        return new BracketItemInfo(from, to, jumpPosition);
    }

    public Integer getItemBeforeBracketsStartPosition(String text) {
        if (openPosition != 0) {
            return SourceAnalyzer.findWordToLeft(text, openPosition-1).getFirst();
        } else {
            return 0;
        }
    }
    
    public Integer getItemIndexByPosition(String text, int position) {
        int itemBeforeBracketsStartPosition = getItemBeforeBracketsStartPosition(text);

        // if out of brackets elements
        if (position < itemBeforeBracketsStartPosition || position > closePosition + 1) {
            return null;
        }
        
        // if -1
        if (position >= itemBeforeBracketsStartPosition && position <= openPosition) {
            return -1;
        }
        
        // if n+1
        if (position == closePosition + 1) {
            return comaPositions.size() + 1;
        }
        
        // if 0 - n-1
        int from = openPosition + 1;
        for (int i = 0; i < comaPositions.size(); i++) {
            int to = comaPositions.get(i);
            if (position >= from && position <= to) {
                return i;
            }
            from = comaPositions.get(i) + 1;
        }
        
        // if n
        if (position >= from && position <= closePosition) {
            return comaPositions.size();
        }

        return null;
    }
    
    public int getLastItemIndex() { 
        return getComaPositions().size() + 1;
    }

}


