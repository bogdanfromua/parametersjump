package com.plb.parametersjump;

import com.intellij.openapi.util.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiFunction;
import java.util.function.Function;


public class SourceAnalyzer {

    //================================================================================
    // analyzingBorder

    private static final int MAX_RANGE = 250;

    public static int getLeftAnalyzingBorder(String text, int position) {
        return Math.max(0, position - MAX_RANGE);
    }

    public static int getRightAnalyzingBorder(String text, int position) {
        return Math.min(text.length(), position + MAX_RANGE);
    }

    public static int getLeftAnalyzingBorder(String text, int position, boolean singleLine) {
        int leftAnalyzingBorder = getLeftAnalyzingBorder(text, position);
        if (singleLine) {
            Integer lineEnd = SearchLineEndLeft(text, leftAnalyzingBorder, position - 1);
            return null != lineEnd
                    ? lineEnd
                    : leftAnalyzingBorder;
        } else {
            return leftAnalyzingBorder;
        }
    }

    public static int getRightAnalyzingBorder(String text, int position, boolean singleLine) {
        int rightAnalyzingBorder = getRightAnalyzingBorder(text, position);
        if (singleLine) {
            Integer lineEnd = SearchLineEndRight(text, position, rightAnalyzingBorder);
            return null != lineEnd
                    ? lineEnd
                    : rightAnalyzingBorder;
        } else {
            return rightAnalyzingBorder;
        }

    }

    private static Integer SearchLineEndLeft(String text, int from, int to) {
        for (int i = to; i >= from; i--){
            char c = text.charAt(i);
            if (c == '\n') {
                return i;
            }
        }
        return null;
    }

    private static Integer SearchLineEndRight(String text, int from, int to) {
        for (int i = from; i <= to; i++){
            char c = text.charAt(i);
            if (c == '\n') {
                return i;
            }
        }
        return null;
    }


    //================================================================================
    // findBracketsAndComas

    public static BracketInfo findBracketsAndComasAround(String text, int position) {
        int leftBorder = getLeftAnalyzingBorder(text, position);
        int rightBorder = getRightAnalyzingBorder(text, position);
        return findBracketsAndComasAround(text, position, leftBorder, rightBorder);
    }

    public static BracketInfo findBracketsAndComasLeft(String text, int position) {
        int leftBorder = getLeftAnalyzingBorder(text, position);
        return findBracketsAndComasLeft(text, position, leftBorder);
    }

    public static BracketInfo findBracketsAndComasRight(String text, int position) {
        int rightBorder = getRightAnalyzingBorder(text, position);
        return findBracketsAndComasRight(text, position, rightBorder);
    }

    public static BracketInfo findBracketsAndComasAround(String text, int position, int leftBorder, int rightBorder) {
        Pair<Integer, Integer> brackets = findBracketsAround(text, position, leftBorder, rightBorder);
        if (null == brackets) {
            return null;
        }
        List<Integer> comas = findComasOnSameLevel(text, brackets.getFirst() + 1, brackets.getSecond() - 1);

        return new BracketInfo(brackets.getFirst(), brackets.getSecond(), comas);
    }

    public static BracketInfo findBracketsAndComasLeft(String text, int position, int leftBorder) {
        Pair<Integer, Integer> brackets = findBracketsLeft(text, position, leftBorder);
        if (null == brackets) {
            return null;
        }
        List<Integer> comas = findComasOnSameLevel(text, brackets.getFirst() + 1, brackets.getSecond() - 1);

        return new BracketInfo(brackets.getFirst(), brackets.getSecond(), comas);
    }

    public static BracketInfo findBracketsAndComasRight(String text, int position, int rightBorder) {
        Pair<Integer, Integer> brackets = findBracketsRight(text, position, rightBorder);
        if (null == brackets) {
            return null;
        }
        List<Integer> comas = findComasOnSameLevel(text, brackets.getFirst() + 1, brackets.getSecond() - 1);

        return new BracketInfo(brackets.getFirst(), brackets.getSecond(), comas);
    }


    //================================================================================
    // findComas

    /**
     * @param from,to - inclusively
     */
    private static List<Integer> findComasOnSameLevel(String text, int from, int to) {
        List<Integer> comas = new ArrayList<>();

        Integer prevPosition = from - 1;

        do {
            prevPosition = findSymbolSingleLevelRight(text, prevPosition + 1, to, ',');
            if (null != prevPosition) {
                comas.add(prevPosition);
            }
        } while (prevPosition != null);

        return comas;
    }


    //================================================================================
    // findBrackets

    private static Pair<Integer, Integer> findBracketsLeft(String text, int position, int leftBorder) {
        AtomicReference<Integer> open = new AtomicReference<>();
        AtomicReference<Integer> close = new AtomicReference<>();

        close.set(findSymbolLeft(text, position - 1, leftBorder, ')'));

        if (close.get() == null) {
            return null;
        }

        open.set(findSymbolSingleLevelLeft(text, close.get() - 1, leftBorder, '('));

        return Pair.create(open.get(), close.get());
    }

    private static Pair<Integer, Integer> findBracketsRight(String text, int position, int rightBorder) {
        AtomicReference<Integer> open = new AtomicReference<>();
        AtomicReference<Integer> close = new AtomicReference<>();

        open.set(findSymbolRight(text, position, rightBorder, '('));

        if (open.get() == null) {
            return null;
        }

        close.set(findSymbolSingleLevelRight(text, open.get() + 1, rightBorder, ')'));

        return Pair.create(open.get(), close.get());
    }

    private static Pair<Integer, Integer> findBracketsAround(String text, int position, int leftBorder, int rightBorder) {
        AtomicReference<Integer> open = new AtomicReference<>();
        AtomicReference<Integer> close = new AtomicReference<>();

        open.set(findSymbolSingleLevelLeft(text, position - 1, leftBorder, '('));

        if (open.get() == null) {
            return null;
        }

        close.set(findSymbolSingleLevelRight(text, position, rightBorder, ')'));

        return Pair.create(open.get(), close.get());
    }


    //================================================================================
    // findSymbol

    public static Integer findSymbolLeft(String text, int from, int to, char symbol) {
        for (int i = from; i > to && !positionOutOfText(text, i); i--) {
            if (text.charAt(i) == symbol) {
                return i;
            }
        }
        return null;
    }

    public static Integer findSymbolRight(String text, int from, int to, char symbol) {
        for (int i = from; i < to && !positionOutOfText(text, i); i++) {
            if (text.charAt(i) == symbol) {
                return i;
            }
        }
        return null;
    }

    private static Integer findSymbolSingleLevelLeft(String text, int from, int to, char symbol) {
        AtomicReference<Integer> position = new AtomicReference<>();
        iterateSingleLevelLeft(text, from, to, (c, i) -> {
            if (c == symbol) {
                position.set(i);
                return false;
            }
            return true;
        });

        return position.get();
    }

    private static Integer findSymbolSingleLevelRight(String text, int from, int to, char symbol) {
        AtomicReference<Integer> position = new AtomicReference<>();
        iterateSingleLevelRight(text, from, to, (c, i) -> {
            if (c == symbol) {
                position.set(i);
                return false;
            }
            return true;
        });

        return position.get();
    }


    //================================================================================
    // iterate

    /**
     * @param from,to - inclusively
     */
    private static void iterateSingleLevelLeft(String text, int from, int to, BiFunction<Character, Integer, Boolean> f) {
        iterateSingleLevel(text, from, to, f, ')', '(', (i)->i-1, (i, finish) -> i < finish);
    }

    /**
     * @param from,to - inclusively
     */
    private static void iterateSingleLevelRight(String text, int from, int to, BiFunction<Character, Integer, Boolean> f) {
        iterateSingleLevel(text, from, to, f, '(', ')', (i)->i+1, (i, finish) -> i > finish);
    }

    private static void iterateSingleLevel(String text, int from, int to, BiFunction<Character, Integer, Boolean> f, char levelUp, char levelDown, Function<Integer, Integer> positionModifier, BiFunction<Integer, Integer, Boolean> finishChecker) {
        int level = 0;
        for (int i = from; !finishChecker.apply(i, to) && !positionOutOfText(text, i); i = positionModifier.apply(i) ) {
            char c = text.charAt(i);
            if (c == levelUp) {
                level++;
            } else if (level > 0 && c == levelDown) {
                level--;
            } else if (0 == level) {
                if (!f.apply(c, i)) {
                    break;
                }
            }
        }
    }


    //================================================================================

    private static boolean positionOutOfText(String text, int position) {
        return (position < 0) || (position >= text.length());
    }


    //================================================================================
    // whitespaces analysing

    private static boolean isWhitespace(char c) {
        return c == ' '  || c == '\n' || c == '\t' ;
    }

    public static int findFirstWordPositionLeft(String text, int from, int to) {
        // find first not whitespace
        int firstWordEnd;

        for (firstWordEnd = to; firstWordEnd >= from ; firstWordEnd--) {
            if (!isWhitespace(text.charAt(firstWordEnd))) {
                break;
            }
        }

        // find first whitespace after not whitespace
        int firstWordPosition = firstWordEnd;
        for (; firstWordPosition > from ; firstWordPosition--) {
            if (isWhitespace(text.charAt(firstWordPosition))) {
                firstWordPosition++;
                break;
            }
        }

        return firstWordPosition;
    }

    public static int findBestParameterJumpPosition(String text, int from, int to, boolean isInsideBrackets) {
        int firstWordPosition;

        // find first not whitespace
        for (firstWordPosition = from; firstWordPosition <= to ; firstWordPosition++) {
            if (!isWhitespace(text.charAt(firstWordPosition))) {
                break;
            }
        }

        int nextWordPosition = firstWordPosition;
        boolean foundNextWord = false;

        if (isInsideBrackets) {
            boolean foundWhitespace = false;

            // find first whitespace after not whitespace
            for (; nextWordPosition <= to ; nextWordPosition++) {
                if (isWhitespace(text.charAt(nextWordPosition))) {
                    foundWhitespace = true;
                    break;
                }
            }

            // find first not whitespace after whitespace
            for (; nextWordPosition <= to ; nextWordPosition++) {
                if (!isWhitespace(text.charAt(nextWordPosition))) {
                    foundNextWord = true;
                    break;
                }
            }
        }

        return foundNextWord ? nextWordPosition: firstWordPosition;
    }

    /**
     */
    public static Pair<Integer, Integer> findWordToLeft(String text, int fromPosition) {
        int leftBorderPosition = fromPosition - MAX_RANGE;
        leftBorderPosition = leftBorderPosition < 0
                ? 0
                : leftBorderPosition;

        int i;
        for (i = fromPosition; i >= leftBorderPosition; i--) {
            char c = text.charAt(i);
            if (c != ' ') {
                break;
            }
        }
        int j;
        for (j = i; j >= leftBorderPosition; j--) {
            char c = text.charAt(j);
            if (!Character.isLetterOrDigit(c)) {
                break;
            }
        }

        if (j < i) {
            j++;
        } else { // there no even single letter or digit
            i = fromPosition;
            j = fromPosition;
        }

        return new Pair<>(j, i);
    }
}
